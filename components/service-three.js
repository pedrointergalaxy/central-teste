import React from "react";
import { Col, Container, Row } from "react-bootstrap";
import { ServicePostThreeData } from "@/data";
import { CgBrands } from "@/data";
// import ServiceCardThree from "@/components/service-card-three";

const ServiceThree = () => {
  const { sectionContent, posts } = ServicePostThreeData;

  const { academy, editora, music, negocios } = CgBrands;

  return (
    <section className="commonSection banner-brand-white">
      <Container>
        <Row>
          <Col lg={12} className="text-center">
            <h2 className="sec_title pb-5">
              Noss<span>as Ma</span>rcas
            </h2>
          </Col>
        </Row>
        <Row>
          <Col lg={3} md={12}>
            <div className="text-right">
              <h5 className="colorBlackBrands titleCardsCG">
                Central Gospel <br /> Academy
              </h5>
              <p className="textContentCG">
                Cursos EAD e presenciais para o desenvolvimento de pessoas nas
                diversas áreas da vida.
              </p>
              <button className="btnYellow">SAIBA MAIS</button>
            </div>
          </Col>
          <Col lg={3} md={12} className="mobile_mb_1 mobile_mt_1 text-right">
            <img src={academy} alt="academy" width="270" />
          </Col>
          <Col lg={3} md={12} className="mobile_mb_1 text-left">
            <img src={editora} alt="editora" width="270" />
          </Col>
          <Col lg={3} md={12}>
            <div>
              <h5 className="colorBlackBrands titleCardsCG">
                Central Gospel <br />{" "}
                <span className="colorYellowBrands">Editora</span>
              </h5>
              <p className="textContentCG">
                Cursos EAD e presenciais para o desenvolvimento de pessoas nas
                diversas áreas da vida.
              </p>
              <button className="btnYellow">SAIBA MAIS</button>
            </div>
          </Col>
        </Row>
        <Row className="mt-5">
          <Col lg={3} md={12}>
            <div className="text-right">
              <h5 className="colorBlackBrands titleCardsCG">
                Central Gospel <br />
                <span className="colorOrangeBrands">Negócios</span>
              </h5>
              <p className="textContentCG">
                Empreendedorismo para alcançar uma vida plena
              </p>
              <button className="btnYellow">SAIBA MAIS</button>
            </div>
          </Col>
          <Col lg={3} md={12} className="mobile_mb_1 mobile_mt_1 text-right">
            <img src={negocios} alt="negocios" width="270" />
          </Col>
          <Col lg={3} md={12} className="mobile_mb_1 text-left">
            <img src={music} alt="music" width="270" />
          </Col>
          <Col lg={3} md={12}>
            <div>
              <h5 className="colorBlackBrands titleCardsCG">
                Central Gospel <br />{" "}
                <span className="colorRedBrands">Music</span>
              </h5>
              <p className="textContentCG">
                Cursos EAD e presenciais para o desenvolvimento de pessoas nas
                diversas áreas da vida.
              </p>
              <button class="btnYellow btn-1 hover-filled-slide-left">
                <span>SAIBA MAIS</span>
              </button>
            </div>
          </Col>
        </Row>
      </Container>
    </section>
  );
};

export default ServiceThree;
