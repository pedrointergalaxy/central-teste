import React from "react";
import { useState } from "react";

const ContactForm = () => {
  const [name, setName] = useState("");
  const [phone, setPhone] = useState("");
  const [email, setEmail] = useState("");
  const [message, setMessage] = useState("");
  const [messageSend, setMessageSend] = useState("");
  const [submitted, setSubmitted] = useState(false);

  const handleSubmit = (e) => {
    e.preventDefault();
    console.log("Enviado!");
    let data = {
      name,
      phone,
      email,
      message,
    };
    fetch("/api/contact", {
      method: "POST",
      headers: {
        Accept: "application/json, text/plain, */*",
        "Content-Type": "application/json",
      },
      body: JSON.stringify(data),
    }).then((res) => {
      console.log("Response received");
      if (res.status === 200) {
        setMessageSend("Contato enviado!");
        console.log("Response succeeded!");
        setSubmitted(true);
        setName("");
        setPhone("");
        setEmail("");
        setMessage("");
      } else {
        setMessageSend("Erro ao enviar contato!");
      }
    });
  };

  return (
    <section className="commonSection ContactPage">
      <div className="container">
        <div className="row">
          <div className="col-lg-6 col-sm-12 col-md-10 ">
            <h2 className="sec_title_contact text-left mb-5">
              F<span>ale Conosc</span>o
            </h2>
            <form
              action="#"
              method="post"
              className="contactFrom"
              id="contactForm"
            >
              <div className="row">
                <div className="col-lg-8 col-sm-12 text-left">
                  <label className="color-black-form font-weight-bold">
                    Nome Completo
                  </label>
                  <input
                    className="input-form required"
                    type="text"
                    onChange={(e) => {
                      setName(e.target.value);
                    }}
                    name="name"
                  />
                </div>
                <div className="col-lg-8 col-sm-12 text-left">
                  <label className="color-black-form font-weight-bold">
                    Telefone (com DDD)
                  </label>
                  <input
                    className="input-form"
                    type="text"
                    onChange={(e) => {
                      setPhone(e.target.value);
                    }}
                    name="phone"
                  />
                </div>
                <div className="col-lg-8 col-sm-12 text-left">
                  <label className="color-black-form font-weight-bold">
                    E-mail
                  </label>
                  <input
                    className="input-form required"
                    type="email"
                    onChange={(e) => {
                      setEmail(e.target.value);
                    }}
                    name="email"
                  />
                </div>
                <div className="col-lg-12 col-sm-12 text-left">
                  <label className="color-black-form font-weight-bold">
                    Mensagem
                  </label>
                  <textarea
                    className="input-form required"
                    onChange={(e) => {
                      setMessage(e.target.value);
                    }}
                    name="message"
                  ></textarea>
                </div>
              </div>
              <button
                className="common_btn float-left mobile_mb_1"
                type="submit"
                onClick={(e) => {
                  handleSubmit(e);
                }}
              >
                <span>ENVIAR</span>
              </button>
              <h3>{messageSend}</h3>
            </form>
          </div>

          <div className="col-lg-6 col-sm-12 col-md-10 ">
            <h2 className="sec_title_contact text-left mb-5">
              A<span>tendiment</span>o
            </h2>

            <div className="row">
              <div className="col-lg-12 col-sm-12">
                <p className="mb-5 color-black-form">
                  Nossa equipe de atendimento e suporte está disponível de
                  segunda à sexta-feira, das 9h às 18h.
                </p>
                <p className="mb-0 color-black-form">
                  <i className="fa fa-phone"></i>
                  <span className="ml-1"> SAC: (21) 2598-2019</span>
                </p>
                <p className="mb-0 color-black-form">
                  <i className="fa fa-whatsapp"></i>
                  <span className="ml-1"> (21) 98133-0519</span>
                </p>
                <p className="mb-5 color-black-form">
                  <i className="fa fa-envelope"></i>
                  <span className="ml-1">
                    {" "}
                    suporte@editoracentralgospel.com
                  </span>
                </p>

                <p className="color_yellow font-weight-bold font-attendance">
                  Atendimento aos Sábados (09h às 15h)
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default ContactForm;
