import React from "react";
import { LogoImage } from "@/data";

const Footer = () => {
  const { light } = LogoImage;
  return (
    <footer className="footer_1">
      <div className="container">
        <div className="row">
          <div className="col-lg-4 col-sm-6 col-md-5">
            <aside className="widget aboutwidget">
              <a href="/">
                <img src={light} alt="" />
              </a>
            </aside>
          </div>
          <div className="col-lg-4 col-sm-4 col-md-4">
            <aside className="widget about_widgets">
              <h3 className="widget_title">Grupo Central Gospel</h3>
              <p>
                Somos um grupo. Um grupo com base nos princípios e valores
                cristãos. Acreditamos que podemos mudar o mundo, com foco nas
                pessoas e no seu crescimento.
              </p>
            </aside>
          </div>
          <div className="col-lg-4 col-sm-2 col-md-3">
            <aside className="widget contact_widgets  ">
              <h3 className="widget_title">Contato</h3>
              <p>
                Estrada do Guerenguê, 1851 – Jacarepaguá
                <br />
                Taquara, Rio de Janeiro – RJ
              </p>
              <p>SAC: (21) 2598-2019</p>
              <p>(21) 98133-0519</p>
            </aside>
          </div>
        </div>
        <div className="row">
          <div className="col-lg-12 col-sm-12 text-center">
            <div className="copyright">
              © {new Date().getFullYear()}{" "}
              <a target="_blank" href="https://intergalaxy.io/">
                Intergalaxy
              </a>{" "}
              . All Rights reserved.
            </div>
          </div>
        </div>
      </div>
    </footer>
  );
};

export default Footer;
