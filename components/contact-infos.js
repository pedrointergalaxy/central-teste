import React from "react";
import { ValuesHome } from "@/data";

const ContactInfos = () => {
  return (
    <section className="commonSection client_2 bgPeoples">
      <div className="container space-custom-home">
        <div className="row">
          <div className="col-lg-12 text-center">
            <h2 className="sec_title mb-3 text-center mb-5">
              V<span>ALORE</span>S
            </h2>
          </div>
        </div>
        <div className="row">
          {ValuesHome.map(({ title, infos }, index) => (
            <div
              className="col-lg-4 col-sm-6 col-md-3 text-center mt-5"
              key={`contact-infos-${index}`}
            >
              <div className="singleClient_2">
                <h3 className="font-weight-bold">{title}</h3>
                {infos.map(({ name }, index) => (
                  <p key={`contact-infos-list-${index}`}>{name}</p>
                ))}
              </div>
            </div>
          ))}
        </div>
      </div>
    </section>
  );
};

export default ContactInfos;
